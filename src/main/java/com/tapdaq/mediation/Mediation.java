package com.tapdaq.mediation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Mediation {

    private static final Logger LOG = LoggerFactory.getLogger(Mediation.class);

    public Mediation() {
    }

    public void run() {
        LOG.info("Running");
    }

    public static void main(String[] args) {
        Mediation m = new Mediation();
        m.run();
    }
}
